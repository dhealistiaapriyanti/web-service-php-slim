from sqlalchemy import Boolean, Column, Integer, String, ForeignKey, Column
from database import Base
from sqlalchemy.orm import relationship

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String(50), unique=True)
    password = Column(String(50))
    email = Column(String(50))

class Registrasi(Base):
    __tablename__ = 'registers'

    id = Column(Integer, primary_key=True, index=True)
    nama_lengkap = Column(String(100))
    tipe_identitas = Column(String(100))
    tgl_lahir = Column(String(255))
    no_hp = Column(String(50))
    no_identitas = Column(String(50))
    email = Column(String(50))
    user_id = Column(Integer)

class Login(Base):
    __tablename__ = 'logins'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    login_date = Column(String(50))

class PemesananTiket(Base):
    __tablename__ = 'tickets'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    tiket_id = Column(Integer)
    stasiun_asal = Column(String(100))
    stasiun_tujuan = Column(String(100))
    tanggal_keberangkatan = Column(String(50))
    jumlah_tiket = Column(Integer)
    usia = Column(Integer)
    total_harga = Column(Integer)
    no_identitas = Column(String(255))
    total_harga = Column(Integer)

class Payment(Base):
    __tablename__ = 'payments'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    ticket_id = Column(Integer, ForeignKey('tickets.id'))
    payment_method = Column(String(50))
    payment_date = Column(String(50))

    user = relationship('User', back_populates='payments')
    ticket = relationship('Ticket', back_populates='payments')

class JadwalKereta(Base):
    __tablename__ = 'jadwal_kereta'

    id = Column(Integer, primary_key=True, index=True)
    nama_kereta = Column(String(100))
    stasiun_asal = Column(String(100))
    stasiun_tujuan = Column(String(100))
    waktu_keberangkatan = Column(String(50))
    waktu_tiba = Column(String(50))
    harga = Column(Integer)

class RiwayatPembelian(Base):
    __tablename__ = 'riwayat_pembelian'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    tiket_id = Column(Integer)
    stasiunAsal = Column(String(100))
    stasiunTujuan = Column(String(100))
    tanggalKeberangkatan = Column(String(50))
    jumlahPenumpang = Column(Integer)
    usia = Column(Integer)
    total_harga = Column(Integer)
    noIdentitas = Column(String(255))