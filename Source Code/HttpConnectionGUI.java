import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.URI;

public class HttpConnectionGUI extends JFrame {
    private JTextField urlField;
    private JTextArea responseArea;

    public HttpConnectionGUI() {
        setTitle("HTTP Connection");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        urlField = new JTextField();
        JButton sendButton = new JButton("Send Request");
        responseArea = new JTextArea();

        sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String url = urlField.getText();
                sendHttpRequest(url);
            }
        });

        setLayout(new BorderLayout());
        JPanel inputPanel = new JPanel(new BorderLayout());
        inputPanel.add(new JLabel("URL: "), BorderLayout.WEST);
        inputPanel.add(urlField, BorderLayout.CENTER);
        inputPanel.add(sendButton, BorderLayout.EAST);

        JScrollPane responseScrollPane = new JScrollPane(responseArea);

        add(inputPanel, BorderLayout.NORTH);
        add(responseScrollPane, BorderLayout.CENTER);

        setVisible(true);
    }

    private void sendHttpRequest(String url) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            int statusCode = response.statusCode();
            String responseBody = response.body();
            String result = "Status Code: " + statusCode + "\n\n" + responseBody;
            responseArea.setText(result);
        } catch (Exception e) {
            responseArea.setText("Error occurred: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new HttpConnectionGUI();
            }
        });
    }
}
