from fastapi import FastAPI, HTTPException, Depends, status
from pydantic import BaseModel
from typing import Annotated
import models
from database import engine, SessionLocal
from sqlalchemy.orm import Session

app = FastAPI()
models.Base.metadata.create_all(bind=engine)

class UserRegister(BaseModel):
    nama_lengkap: str
    tipe_identitas: str
    tgl_lahir: str
    no_hp: str
    no_identitas: str
    email: str
    user_id: str

class UserBase(BaseModel):
    username: str
    password: str

class UserLogin(BaseModel):
    username: str
    password: str

class Kereta(BaseModel):
    id: int
    nama: str
    kapasitas: int
    jenis: str
    kelas: str

class Stasiun(BaseModel):
    id: int
    nama: str
    kota: str
    provinsi: str

class JadwalKereta(BaseModel):
    id: int
    nama_kereta: str
    stasiun_asal: str
    stasiun_tujuan: str
    waktu_keberangkatan: str
    waktu_tiba: str
    harga: int

class TicketBase(BaseModel):
    id: int
    user_id: int
    tiket_id: int
    stasiun_asal: str
    stasiun_tujuan: str
    tanggal_keberangkatan: str
    jumlah_tiket: int
    usia: int
    no_identitas: str
    total_harga: int

class Payment(BaseModel):
    user_id: int
    ticket_id: int
    payment_method: str
    payment_date: str

class RiwayatPembelian(BaseModel):
    id: int
    user_id: int
    tiket_id: int
    stasiunAsal: str
    stasiunTujuan: str
    tanggalKeberangkatan: str
    jumlahPenumpang: int
    usia: int
    total_harga: int
    noIdentitas: str

class TopUpKaiPay(BaseModel):
    id: int
    user_id: int
    jumlah_topup: float
    tanggal_transaksi: str

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

db_dependency = Annotated[Session, Depends(get_db)]

@app.get("/posts")
def get_posts(db: db_dependency):
    posts = db.query(models.Post).all()
    return posts

@app.post("/users", status_code=status.HTTP_201_CREATED)
async def create_user(user: UserBase, db: db_dependency):
    db_user = models.User(**user.dict())
    db.add(user)
    db.commit()

@app.get("/users/{user_id}", status_code=status.HTTP_200_OK)
async def read_user(user_id: int, db: db_dependency):
    user = db.query(models.User).filter(models.User.id == user_id).first()
    if user is None:
        raise HTTPException(status_code=404, detail = 'User not found')
    return user

@app.put("/users/{user_id}", status_code=status.HTTP_200_OK)
async def update_user(user_id: int, updated_user: UserBase, db: db_dependency):
    db_user = db.query(models.User).filter(models.User.id == user_id).first()
    if db_user is None:
        raise HTTPException(status_code=404, detail='User not found')
    
    # Update the user attributes with the values from updated_user
    for key, value in updated_user.dict().items():
        setattr(db_user, key, value)
    
    db.commit()
    return db_user

@app.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(user_id: int, db: db_dependency):
    db_user = db.query(models.User).filter(models.User.id == user_id).first()
    if db_user is None:
        raise HTTPException(status_code=404, detail='User not found')
    
    db.delete(db_user)
    db.commit()
    return None

@app.post("/register", status_code=status.HTTP_201_CREATED)
async def register_user(user: UserRegister, db: db_dependency):
    # Cek apakah pengguna dengan username yang sama sudah terdaftar
    existing_user = db.query(models.User).filter(models.User.username == user.username).first()
    if existing_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    
    # Membuat objek pengguna baru
    new_user = models.User(username=user.username, password=user.password, email=user.email)
    
    # Menyimpan pengguna ke database
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    
    return {"message": "User registered successfully"}

@app.get("/register/{user_id}", status_code=status.HTTP_200_OK)
async def get_user(user_id: int, db: db_dependency):
    user = db.query(models.User).filter(models.User.id == user_id).first()
    if user is None:
        raise HTTPException(status_code=404, detail='User not found')
    return user

@app.put("/register/{user_id}", status_code=status.HTTP_200_OK)
async def update_user(user_id: int, updated_user: UserRegister, db: db_dependency):
    db_user = db.query(models.User).filter(models.User.id == user_id).first()
    if db_user is None:
        raise HTTPException(status_code=404, detail='User not found')
    
    # Update the user attributes with the values from updated_user
    db_user.username = updated_user.username
    db_user.password = updated_user.password
    db_user.email = updated_user.email
    
    db.commit()
    return {"message": "User updated successfully"}

@app.delete("/register/{user_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(user_id: int, db: db_dependency):
    db_user = db.query(models.User).filter(models.User.id == user_id).first()
    if db_user is None:
        raise HTTPException(status_code=404, detail='User not found')
    
    db.delete(db_user)
    db.commit()
    return None

@app.post("/login", status_code=status.HTTP_200_OK)
async def login(user_login: UserLogin, db: db_dependency):
    user = db.query(models.User).filter(models.User.username == user_login.username).first()
    if user is None or user.password != user_login.password:
        raise HTTPException(status_code=401, detail='Invalid username or password')
    return {'message': 'Login successful'}

@app.get("/login/{username}", status_code=status.HTTP_200_OK)
async def get_user(username: str, db: db_dependency):
    user = db.query(models.User).filter(models.User.username == username).first()
    if user is None:
        raise HTTPException(status_code=404, detail='User not found')
    return user

@app.put("/login/{username}", status_code=status.HTTP_200_OK)
async def update_user(username: str, updated_user: UserLogin, db: db_dependency):
    user = db.query(models.User).filter(models.User.username == username).first()
    if user is None:
        raise HTTPException(status_code=404, detail='User not found')
    
    if user.password != updated_user.password:
        raise HTTPException(status_code=401, detail='Invalid password')
    
    # Update the user's password
    user.password = updated_user.password
    
    db.commit()
    return {'message': 'Password updated successfully'}

@app.delete("/login/{username}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(username: str, db: db_dependency):
    user = db.query(models.User).filter(models.User.username == username).first()
    if user is None:
        raise HTTPException(status_code=404, detail='User not found')
    
    db.delete(user)
    db.commit()
    return None

@app.get("/kereta", status_code=status.HTTP_200_OK)
async def get_kereta(db: db_dependency):
    kereta = db.query(models.Kereta).all()
    return kereta

@app.get("/kereta/{kereta_id}", status_code=status.HTTP_200_OK)
async def get_kereta_by_id(kereta_id: int, db: db_dependency):
    kereta = db.query(models.Kereta).filter(models.Kereta.id == kereta_id).first()
    if kereta is None:
        raise HTTPException(status_code=404, detail='Kereta not found')
    return kereta

@app.post("/kereta", status_code=status.HTTP_201_CREATED)
async def create_kereta(kereta: Kereta, db: db_dependency):
    db_kereta = models.Kereta(**kereta.dict())
    db.add(db_kereta)
    db.commit()
    db.refresh(db_kereta)
    return {"message": "Kereta created successfully"}

@app.put("/kereta/{kereta_id}", status_code=status.HTTP_200_OK)
async def update_kereta(kereta_id: int, kereta: Kereta, db: db_dependency):
    existing_kereta = db.query(models.Kereta).filter(models.Kereta.id == kereta_id).first()
    if existing_kereta is None:
        raise HTTPException(status_code=404, detail='Kereta not found')

    existing_kereta.nama = kereta.nama
    existing_kereta.kapasitas = kereta.kapasitas
    existing_kereta.jenis = kereta.jenis
    existing_kereta.kelas = kereta.kelas

    db.commit()

    return existing_kereta

@app.delete("/kereta/{kereta_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_kereta(kereta_id: int, db: db_dependency):
    kereta = db.query(models.Kereta).filter(models.Kereta.id == kereta_id).first()
    if not kereta:
        raise HTTPException(status_code=404, detail='Kereta not found')

    db.delete(kereta)
    db.commit()

    return None

@app.get("/stasiun", status_code=status.HTTP_200_OK)
async def get_stasiun(db: db_dependency):
    stasiun = db.query(models.Stasiun).all()
    return stasiun

@app.post("/stasiun", status_code=status.HTTP_201_CREATED)
async def create_stasiun(stasiun: Stasiun, db: db_dependency):
    db_stasiun = models.Stasiun(**stasiun.dict())
    db.add(db_stasiun)
    db.commit()
    db.refresh(db_stasiun)
    return {"message": "Stasiun created successfully"}

@app.put("/stasiun/{stasiun_id}", status_code=status.HTTP_200_OK)
async def update_stasiun(stasiun_id: int, updated_stasiun: Stasiun, db: db_dependency):
    db_stasiun = db.query(models.Stasiun).filter(models.Stasiun.id == stasiun_id).first()
    if db_stasiun is None:
        raise HTTPException(status_code=404, detail='Stasiun not found')
    
    # Update the stasiun attributes with the values from updated_stasiun
    for key, value in updated_stasiun.dict().items():
        setattr(db_stasiun, key, value)
    
    db.commit()
    return {"message": "Stasiun updated successfully"}

@app.delete("/stasiun/{stasiun_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_stasiun(stasiun_id: int, db: db_dependency):
    db_stasiun = db.query(models.Stasiun).filter(models.Stasiun.id == stasiun_id).first()
    if db_stasiun is None:
        raise HTTPException(status_code=404, detail='Stasiun not found')
    
    db.delete(db_stasiun)
    db.commit()
    return None

@app.get("/jadwal_kereta", status_code=status.HTTP_200_OK)
async def get_jadwal_kereta(db: db_dependency):
    jadwal_kereta = db.query(models.JadwalKereta).all()
    return jadwal_kereta

@app.get("/jadwal_kereta/{jadwal_id}", status_code=status.HTTP_200_OK)
async def get_jadwal_kereta_by_id(jadwal_id: int, db: db_dependency):
    jadwal_kereta = db.query(models.JadwalKereta).filter(models.JadwalKereta.id == jadwal_id).first()
    if jadwal_kereta is None:
        raise HTTPException(status_code=404, detail='Jadwal kereta not found')
    return jadwal_kereta

@app.post("/jadwal_kereta", status_code=status.HTTP_201_CREATED)
async def create_jadwal_kereta(jadwal_kereta: JadwalKereta, db: db_dependency):
    db_jadwal_kereta = models.JadwalKereta(**jadwal_kereta.dict())
    db.add(db_jadwal_kereta)
    db.commit()
    db.refresh(db_jadwal_kereta)
    return {"message": "Jadwal kereta created successfully"}

@app.put("/jadwal_kereta/{jadwal_id}", status_code=status.HTTP_200_OK)
async def update_jadwal_kereta(jadwal_id: int, jadwal_kereta: JadwalKereta, db: db_dependency):
    existing_jadwal_kereta = db.query(models.JadwalKereta).filter(models.JadwalKereta.id == jadwal_id).first()
    if existing_jadwal_kereta is None:
        raise HTTPException(status_code=404, detail='Jadwal kereta not found')
    
    existing_jadwal_kereta.nama_kereta = jadwal_kereta.nama_kereta
    existing_jadwal_kereta.stasiun_asal = jadwal_kereta.stasiun_asal
    existing_jadwal_kereta.stasiun_tujuan = jadwal_kereta.stasiun_tujuan
    existing_jadwal_kereta.waktu_keberangkatan = jadwal_kereta.waktu_keberangkatan
    existing_jadwal_kereta.waktu_tiba = jadwal_kereta.waktu_tiba
    existing_jadwal_kereta.harga = jadwal_kereta.harga
    
    db.commit()
    
    return existing_jadwal_kereta

@app.delete("/jadwal_kereta/{jadwal_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_jadwal_kereta(jadwal_id: int, db: db_dependency):
    jadwal_kereta = db.query(models.JadwalKereta).filter(models.JadwalKereta.id == jadwal_id).first()
    if not jadwal_kereta:
        raise HTTPException(status_code=404, detail='Jadwal kereta not found')
    
    db.delete(jadwal_kereta)
    db.commit()
    
    return None

@app.get("/tickets/{ticket_id}", status_code=status.HTTP_200_OK)
async def get_ticket(ticket_id: int, db: db_dependency):
    ticket = db.query(models.Ticket).filter(models.Ticket.id == ticket_id).first()
    if not ticket:
        raise HTTPException(status_code=404, detail="Ticket not found")
    
    return ticket

@app.post("/tickets", status_code=status.HTTP_201_CREATED)
async def create_ticket(ticket: TicketBase, db: db_dependency):
    # Validasi login pengguna
    user = db.query(models.User).filter(models.User.id == ticket.user_id).first()
    if user is None:
        raise HTTPException(status_code=401, detail='User not logged in')

    # Proses pemesanan tiket
    db_ticket = models.Ticket(**ticket.dict())
    db.add(db_ticket)
    db.commit()

    return {'message': 'Ticket booked successfully'}

@app.delete("/tickets/{ticket_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_ticket(ticket_id: int, db: db_dependency):
    ticket = db.query(models.Ticket).filter(models.Ticket.id == ticket_id).first()
    if not ticket:
        raise HTTPException(status_code=404, detail="Ticket not found")
    
    db.delete(ticket)
    db.commit()
    
    return None

@app.put("/tickets/{ticket_id}", status_code=status.HTTP_200_OK)
async def update_ticket(ticket_id: int, ticket: TicketBase, db: db_dependency):
    existing_ticket = db.query(models.Ticket).filter(models.Ticket.id == ticket_id).first()
    if not existing_ticket:
        raise HTTPException(status_code=404, detail="Ticket not found")
    
    # Update ticket properties with the new values
    existing_ticket.stasiunAsal = ticket.stasiunAsal
    existing_ticket.stasiunTujuan = ticket.stasiunTujuan
    existing_ticket.tanggalKeberangkatan = ticket.tanggalKeberangkatan
    existing_ticket.jumlahPenumpang = ticket.jumlahPenumpang
    existing_ticket.usia = ticket.usia
    existing_ticket.noIdentitas = ticket.noIdentitas
    
    db.commit()
    
    return existing_ticket

@app.post("/payments", status_code=status.HTTP_201_CREATED)
async def create_payment(payment: Payment, db: db_dependency):
    # Validasi login pengguna
    user = db.query(models.User).filter(models.User.id == payment.user_id).first()
    if user is None:
        raise HTTPException(status_code=401, detail='User not logged in')

    # Validasi tiket
    ticket = db.query(models.Ticket).filter(models.Ticket.id == payment.ticket_id).first()
    if ticket is None:
        raise HTTPException(status_code=404, detail='Ticket not found')

    # Proses pembayaran
    db_payment = models.Payment(
        user_id=payment.user_id,
        ticket_id=payment.ticket_id,
        payment_method=payment.payment_method,
        payment_date=payment.payment_date
    )
    db.add(db_payment)
    db.commit()

    return {'message': 'Payment successful'}

@app.get("/payments/{payment_id}", status_code=status.HTTP_200_OK)
async def get_payment(payment_id: int, db: db_dependency):
    payment = db.query(models.Payment).filter(models.Payment.id == payment_id).first()
    if payment is None:
        raise HTTPException(status_code=404, detail='Payment not found')
    
    return payment

@app.put("/payments/{payment_id}", status_code=status.HTTP_200_OK)
async def update_payment(payment_id: int, updated_payment: Payment, db: db_dependency):
    db_payment = db.query(models.Payment).filter(models.Payment.id == payment_id).first()
    if db_payment is None:
        raise HTTPException(status_code=404, detail='Payment not found')
    
    # Update the payment attributes with the values from updated_payment
    for key, value in updated_payment.dict().items():
        setattr(db_payment, key, value)
    
    db.commit()
    return db_payment

@app.delete("/payments/{payment_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_payment(payment_id: int, db: db_dependency):
    db_payment = db.query(models.Payment).filter(models.Payment.id == payment_id).first()
    if db_payment is None:
        raise HTTPException(status_code=404, detail='Payment not found')
    
    db.delete(db_payment)
    db.commit()
    return None

@app.get("/riwayat_pembelian", status_code=status.HTTP_200_OK)
async def get_riwayat_pembelian(db: db_dependency):
    riwayat = db.query(models.RiwayatPembelian).all()
    return riwayat

@app.post("/riwayat_pembelian", status_code=status.HTTP_201_CREATED)
async def create_riwayat_pembelian(riwayat: RiwayatPembelian, db: db_dependency):
    db_riwayat = models.RiwayatPembelian(**riwayat.dict())
    db.add(db_riwayat)
    db.commit()
    return {'message': 'Riwayat pembelian created successfully'}

@app.put("/riwayat_pembelian/{riwayat_id}", status_code=status.HTTP_200_OK)
async def update_riwayat_pembelian(riwayat_id: int, updated_riwayat: RiwayatPembelian, db: db_dependency):
    db_riwayat = db.query(models.RiwayatPembelian).filter(models.RiwayatPembelian.id == riwayat_id).first()
    if db_riwayat is None:
        raise HTTPException(status_code=404, detail='Riwayat pembelian not found')

    # Update the riwayat_pembelian attributes with the values from updated_riwayat
    for key, value in updated_riwayat.dict().items():
        setattr(db_riwayat, key, value)

    db.commit()
    return db_riwayat

@app.delete("/riwayat_pembelian/{riwayat_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_riwayat_pembelian(riwayat_id: int, db: db_dependency):
    db_riwayat = db.query(models.RiwayatPembelian).filter(models.RiwayatPembelian.id == riwayat_id).first()
    if db_riwayat is None:
        raise HTTPException(status_code=404, detail='Riwayat pembelian not found')

    db.delete(db_riwayat)
    db.commit()
    return None

@app.get("/topup", status_code=status.HTTP_200_OK)
async def get_topup(db: db_dependency):
    topup = db.query(models.TopUpKaiPay).all()
    return topup

@app.post("/topup", status_code=status.HTTP_201_CREATED)
async def create_topup(topup: TopUpKaiPay, db: db_dependency):
    db_topup = models.TopUpKaiPay(**topup.dict())
    db.add(db_topup)
    db.commit()
    db.refresh(db_topup)
    return {"message": "Top-up created successfully"}

@app.put("/topup/{topup_id}", status_code=status.HTTP_200_OK)
async def update_topup(topup_id: int, updated_topup: TopUpKaiPay, db: db_dependency):
    db_topup = db.query(models.TopUpKaiPay).filter(models.TopUpKaiPay.id == topup_id).first()
    if db_topup is None:
        raise HTTPException(status_code=404, detail='Top-up not found')
    
    # Update the top-up attributes with the values from updated_topup
    for key, value in updated_topup.dict().items():
        setattr(db_topup, key, value)
    
    db.commit()
    return {"message": "Top-up updated successfully"}

@app.delete("/topup/{topup_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_topup(topup_id: int, db: db_dependency):
    db_topup = db.query(models.TopUpKaiPay).filter(models.TopUpKaiPay.id == topup_id).first()
    if db_topup is None:
        raise HTTPException(status_code=404, detail='Top-up not found')
    
    db.delete(db_topup)
    db.commit()
    return None