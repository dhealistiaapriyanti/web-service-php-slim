# No.1
**Mampu menunjukkan keseluruhan Use Case beserta rangking dari setiap Use Case dari produk digital:**
- Use Case User

Pada produk digital yang saya pilih yaitu KAI Access terdapat 48 Use Case User yang terdiri dari Registrasi pengguna, log in, lupa password, ganti password, detail akun, informasi akun, pengaturan akun, upgrade, lihat jadwal kereta, pilih jenis kereta, pilih kelas kereta, pesan tiket, pilih tanggal keberangkatan, pilih stasiun asal, pilih stasiun tujuan, pilih jumlah penumpang, pilih tempat duduk, bayar tiket, metode pembayaran, tambahkan tiket, cek pemesanan, pembatalan tiket, ubah jadwal, KA bandara, informasi KRL, posisi kereta, jadwal kereta, rute kereta, jalur kereta, tarif kereta, KAI logistik, cek tarif KAI logistik, detail pengiriman, informasi outlet, cek resi, top up & tagihan, top up pulsa, top up paket data, top up token listrik, top up KAIPay, pemesanan bus, tiket, histori pembelian tiket, daftar riwayat perjalanan, pilihan bahasa, berita, notifikasi, log out.

- Use Case manajemen perusahan 

Pada use case manajemen perusahan terdapat 24 use case yang terdiri dari, manajemen pengguna, manajemen, jadwal kereta, manajemen tarif kereta, manajemen, stasiun, manajemen pengumuman, manajemen notifikasi, manajemen promo, manajemen metode pembelian, manajemen hak akses, manajemen pengelola konten, manajemen analisis data, manajemen pemeliharaan aplikasi, manajemen pengelola keamanan, manajemen integrasi, manajemen pemantauan kerja, manajemen layanan, manajemen tempat duduk, manajemen pelaporan, manajemen riwayat perjalanan, manajemen umpan balik, manajemen riwayat transaksi, manajemen backup dan restore, manajemen perangkat dan aksesibilitas, manajemen tampilan aplikasi.
- Use Case direksi perusahan (dashboard, monitoring, analisi).

pada Use Case direksi perusahan (dashboard, monitoring, analisi) terdapat 28 use case yang terdiri dari, monitoring kinerja aplikasi, analisi pengguna dan segment, optimisasi fitur, pengukuran efektivitas, pengawasan kualitas layanan, pengambilan keputusan, keamanan data, analisis kompetitif, pengembagan bisnis, pengukuran dampak dan ROI, prediksi dan peramalan, responsif terhadap umpan balik pengguna, pengembangan tim, strategi monetisasi, pengelolaan pengembangan produk, pengelaman pengguna multichannel, kemitraan dan analisi, pengembangan pasar internasional, analisis sentimen dan reputasi, perbaikan berkelanjutan, pengembangan keterlibatan pengguna, personalisasi pengalaman pengguna, pengguna teknologi AI dan machine learning, peningkatan potensi pengguna, pengembangan komunitas, pengoptimalan proses internal, kolaborasi dengan mitra bisnis, penyempurnaan UI/UX, analisi geografis dan demografis.

Berikut Use Case keseluruhan [Use Case (user, manajemen perusahan, direksi perusahan)](file/Use_Case_Aplikasi_KAI_Access.pdf)

# No.2
**Mampu mendemostrasikan class diagram dari keseluruhan use case produk digital**
Berikut class diagram dari use case [Class Diagram](Gambar/Use_Case_KAI_Access.png)

# No.3
**Mampu menunjukkan dan menjelaskan setiap poin dari SOLID design pattern principle**
Dalam program yang Anda berikan, terdapat implementasi beberapa prinsip SOLID. Berikut adalah penjelasan setiap poin SOLID yang ada dalam program:

1. Single Responsibility Principle (SRP):
   - Setiap kelas memiliki tanggung jawab tunggal yang terdefinisi dengan jelas, seperti kelas UserRegister, UserBase, UserLogin, Kereta, Stasiun, JadwalKereta, TicketBase, Payment, RiwayatPembelian, dan TopUpKaiPay.
   - Setiap fungsi dalam API memiliki tanggung jawab yang terfokus, seperti fungsi create_user, read_user, update_user, delete_user, create_kereta, get_kereta, update_kereta, delete_kereta, create_stasiun, get_stasiun, update_stasiun, delete_stasiun, dan lain-lain.

2. Open-Closed Principle (OCP):
   - Kode tidak perlu dimodifikasi secara langsung ketika menambahkan atau memperluas fitur-fitur baru. Misalnya, jika Anda ingin menambahkan operasi baru pada entitas yang ada, Anda dapat membuat fungsi baru tanpa mengubah fungsi yang sudah ada.

3. Liskov Substitution Principle (LSP):
   - Tidak terlihat secara eksplisit dalam program ini, namun prinsip ini menyatakan bahwa objek dari kelas turunan harus dapat digunakan sebagai pengganti objek dari kelas induk tanpa mempengaruhi kebenaran program.

4. Interface Segregation Principle (ISP):
   - Tidak terlihat secara eksplisit dalam program ini, namun prinsip ini menyatakan bahwa klien tidak boleh dipaksa untuk mengimplementasikan antarmuka yang tidak digunakan. Dalam program ini, antarmuka yang digunakan adalah antarmuka FastAPI, dan setiap fungsi hanya mengimplementasikan operasi yang diperlukan.

5. Dependency Inversion Principle (DIP):
   - Fungsi-fungsi dalam API menerima dependensi pada antarmuka `db_dependency` yang dihasilkan melalui `get_db` yang menggunakan fungsi `yield`. Hal ini mengikuti prinsip injeksi dependensi, di mana dependensi (objek sesi database) disediakan dari luar fungsi. Ini memudahkan pengujian dan memisahkan logika bisnis dari logika akses data.

Meskipun SOLID adalah prinsip yang penting untuk dipertimbangkan dalam pengembangan perangkat lunak, implementasinya dapat bervariasi tergantung pada konteks dan kebutuhan proyek. Penerapan SOLID yang baik dapat meningkatkan keberlanjutan, fleksibilitas, dan pemeliharaan program.

Berikut program lengkapnya [Program main.py Web Service KAI Access](Source Code/main.py)

# No.4
**Mampu menunjukkan dan menjelaskan design pattern yang dipilih**
Dalam program yang saya buat, ada beberapa desain pola yang sesuai untuk dipertimbangkan adalah sebagai berikut:

1. Model-View-Controller (MVC):
   - Pola desain MVC memisahkan aplikasi menjadi tiga komponen utama: Model, View, dan Controller.
   - Model mengelola logika bisnis dan interaksi dengan database.
   - View bertanggung jawab untuk tampilan pengguna.
   - Controller mengendalikan aliran logika aplikasi dan berinteraksi dengan Model dan View.
   - Dengan menggunakan pola desain MVC, Anda dapat memisahkan tugas dan mempermudah pengembangan, pemeliharaan, dan pengujian aplikasi.

2. Repository Pattern:
   - Pola desain Repository terkait dengan akses data ke sumber daya persisten seperti database.
   - Dalam pola ini, setiap entitas memiliki repositori terpisah yang bertanggung jawab atas operasi penyimpanan dan pengambilan data terkait entitas tersebut.
   - Repositori menyediakan antarmuka yang terdefinisi dengan jelas untuk berinteraksi dengan entitas dan menyembunyikan detail akses data ke lapisan lain dalam aplikasi.
   - Dengan menggunakan pola desain Repository, Anda dapat mengisolasi logika akses data dari logika bisnis Anda, memungkinkan fleksibilitas dalam mengganti sumber data atau implementasi akses data tanpa mempengaruhi kode lain.

3. Dependency Injection (DI):
   - Pola desain Dependency Injection (DI) membantu dalam mengelola dependensi antara komponen dalam aplikasi.
   - Dengan menggunakan DI, dependensi eksternal (misalnya, sesi database) disediakan secara terpisah dan diinjeksikan ke komponen yang membutuhkannya.
   - Ini memudahkan pengujian dan memisahkan logika bisnis dari detail implementasi dan dependensi.
   - Dalam program Anda, Anda sudah mengimplementasikan DI dengan menggunakan fungsi `get_db` dan `db_dependency` untuk menyediakan sesi database kepada fungsi-fungsi yang membutuhkannya.

4. Factory Pattern:
   - Jika Anda memiliki kebutuhan untuk membuat objek dengan konfigurasi yang rumit atau dinamis, Factory Pattern dapat berguna.
   - Dalam pola ini, Anda memiliki kelas atau metode Factory yang bertanggung jawab untuk membuat objek sesuai dengan kebutuhan dan konfigurasi tertentu.
   - Dalam program Anda, Anda dapat mempertimbangkan menggunakan Factory Pattern jika Anda memiliki kebutuhan untuk membuat objek-objek dengan konfigurasi yang kompleks, seperti objek Kereta atau Stasiun.

Pilihan desain pola akan tergantung pada kompleksitas dan kebutuhan aplikasi. Saya memilih desain pola yang paling sesuai untuk memisahkan tanggung jawab, meningkatkan fleksibilitas, dan memudahkan pemeliharaan dan pengembangan aplikasi.

Berikut program lengkapnya [Program main.py Web Service KAI Access](Source Code/main.py)

# No.5
**Mampu menunjukkan dan menjelaskan konektivitas ke database**
Dalam pembuatan web service ini saya menggunakan library restfulapi python fastapi, lalu untuk menghubungkan database ke dalam python fastapi ada beberapa langkah diantaranya:
1. Install python yang terdapat pada web resminya
2. pada konektivitas database web service kali ini saya menggunakan sqlite, ini adalah basis data berbasis sistem file yang mudah digunakan dan didukung oleh python. 
3. install sqlite
4. Agar informasi mempermudah pengerjaan buat folder env dengan cara **python -m venv env** 
5. lalu masuk ke directory env dengan cara **source env/Scripts/activate**.
Semua dilakukan didalam cmd/gitbash
6. Selanjutnya saya menggunakan ORM yang disebut sqlachemy, ORM adalah mapper yang membantu menerjemahkan catatan tabel database ke objek kelas dengan cara **pip install fastapi uvicorn sqlachemy pymysql**. 
7. membuat file requirements.txt lalu jalankan **pip install -r requirements.txt** pada cmd
8. buat file database.py dan masukkan konfigurasinya seperti pada program

    from sqlalchemy import Boolean, Float, Column, Integer,   String, ForeignKey, Column
    from database import Base

**from sqlalchemy.orm import relationship
from sqlalchemy import Boolean, Float, Column, Integer, String, ForeignKey** : Melakukan impor beberapa tipe data kolom yang tersedia dalam SQLAlchemy. Tipe data ini digunakan untuk mendefinisikan jenis data yang akan disimpan dalam kolom-kolom tabel.

**Boolea**n: Merepresentasikan nilai boolean (True atau False).
**Float**: Merepresentasikan nilai desimal (floating point).
**Column**: Merepresentasikan sebuah kolom dalam sebuah tabel.
**Integer**: Merepresentasikan nilai bilangan bulat.
**String**: Merepresentasikan nilai string (teks).
**ForeignKey**: Merepresentasikan hubungan antara dua tabel melalui kunci asing (foreign key).

Setelah melakukan impor, Anda dapat mendefinisikan kelas model dengan kolom-kolom dan hubungan yang sesuai. Contoh definisi kelas model menggunakan kolom-kolom yang telah diimpor:

Class User

    class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String(50), unique=True)
    password = Column(String(50))
    email = Column(String(50))

Class User merupakan definisi model untuk tabel "users" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas User:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**username**: Kolom ini bertipe String(50) dan memiliki atribut unique=True, yang menandakan bahwa setiap nilai dalam kolom ini harus unik. Kolom ini digunakan untuk menyimpan nama pengguna (username) dari pengguna.

**password**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan kata sandi (password) dari pengguna.

**email**: Kolom ini bertipe String(50) dan menyimpan alamat email pengguna. Kolom ini tidak memiliki atribut tambahan seperti unique, yang berarti alamat email tidak harus unik dalam tabel.

Dengan definisi ini, dapat menggunakan kelas User untuk memetakan data pengguna ke dalam tabel "users" dalam database. Setiap atribut dalam kelas User akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class Registrasi

    class Registrasi(Base):
    __tablename__ = 'registers'

    id = Column(Integer, primary_key=True, index=True)
    nama_lengkap = Column(String(100))
    tipe_identitas = Column(String(100))
    tgl_lahir = Column(String(255))
    no_hp = Column(String(50))
    no_identitas = Column(String(50))
    email = Column(String(50))
    user_id = Column(Integer)

Class Registrasi adalah definisi model untuk tabel "registers" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas Registrasi:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**nama_lengkap**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama lengkap dari registrasi pengguna.

**tipe_identitas**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan jenis tipe identitas dari registrasi pengguna, seperti KTP, SIM, atau paspor.

**tgl_lahir**: Kolom ini bertipe String(255) dan digunakan untuk menyimpan tanggal lahir dari registrasi pengguna.

**no_hp**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan nomor telepon (handphone) dari registrasi pengguna.

**no_identitas**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan nomor identitas dari registrasi pengguna, seperti nomor KTP atau SIM.

**email**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan alamat email dari registrasi pengguna.

**user_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID pengguna yang terkait dengan registrasi. Ini adalah kolom yang mewakili hubungan antara tabel "registers" dengan tabel pengguna (misalnya, tabel "users").

Dengan definisi ini, dapat menggunakan kelas Registrasi untuk memetakan data registrasi ke dalam tabel "registers" dalam database. Setiap atribut dalam kelas Registrasi akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class Login

    class Login(Base):
    __tablename__ = 'logins'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    username = Column(String(50))
    password = Column(String(50))

Class Login adalah definisi model untuk tabel "logins" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas Login:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**user_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID pengguna yang terkait dengan login. Ini adalah kolom yang mewakili hubungan antara tabel "logins" dengan tabel pengguna (misalnya, tabel "users").

**username**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan nama pengguna (username) dari login.

**password**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan kata sandi (password) dari login.

Dengan definisi ini, dapat menggunakan kelas Login untuk memetakan data login ke dalam tabel "logins" dalam database. Setiap atribut dalam kelas Login akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class PemesananTiket

    class PemesananTiket(Base):
    __tablename__ = 'tickets'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    tiket_id = Column(Integer)
    stasiun_asal = Column(String(100))
    stasiun_tujuan = Column(String(100))
    tanggal_keberangkatan = Column(String(50))
    jumlah_tiket = Column(Integer)
    usia = Column(Integer)
    total_harga = Column(Integer)
    no_identitas = Column(String(255))
    total_harga = Column(Integer)

Class PemesananTiket adalah definisi model untuk tabel "tickets" dalam database:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**user_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID pengguna yang terkait dengan pemesanan tiket.

**tiket_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID tiket yang terkait dengan pemesanan tiket.

**stasiun_asal**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama stasiun asal pemesanan tiket.

**stasiun_tujuan**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama stasiun tujuan pemesanan tiket.

**tanggal_keberangkatan**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan tanggal keberangkatan pemesanan tiket.

jumlah_tiket: Kolom ini bertipe Integer dan digunakan untuk menyimpan jumlah tiket yang dipesan.

**usia**: Kolom ini bertipe Integer dan digunakan untuk menyimpan usia penumpang yang memesan tiket.

**total_harga**: Kolom ini bertipe Integer dan digunakan untuk menyimpan total harga pemesanan tiket.

**no_identitas**: Kolom ini bertipe String(255) dan digunakan untuk menyimpan nomor identitas penumpang yang memesan tiket.

Dengan definisi ini, dapat menggunakan Class PemesananTiket untuk memetakan data pemesanan tiket ke dalam tabel "tickets" dalam database. Setiap atribut dalam kelas PemesananTiket akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class Payment
    class Payment(Base):
    __tablename__ = 'payments'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    ticket_id = Column(Integer, ForeignKey('tickets.id'))
    payment_method = Column(String(50))
    payment_date = Column(String(50))

Class Payment adalah definisi model untuk tabel "payments" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas Payment:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**user_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID pengguna yang terkait dengan pembayaran. Ini adalah kolom yang mewakili hubungan antara tabel "payments" dengan tabel pengguna (misalnya, tabel "users"). Kolom ini menggunakan ForeignKey untuk menunjukkan bahwa nilainya merujuk ke kolom id di tabel "users".

**ticket_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID tiket yang terkait dengan pembayaran. Ini adalah kolom yang mewakili hubungan antara tabel "payments" dengan tabel tiket (misalnya, tabel "tickets"). Kolom ini menggunakan ForeignKey untuk menunjukkan bahwa nilainya merujuk ke kolom id di tabel "tickets".

**payment_method**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan metode pembayaran yang digunakan dalam pembayaran.

**payment_date**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan tanggal pembayaran.

Dengan definisi ini, dapat menggunakan Class Payment untuk memetakan data pembayaran ke dalam tabel "payments" dalam database. Setiap atribut dalam kelas Payment akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class JadwalKereta
    class JadwalKereta(Base):
    __tablename__ = 'jadwal_kereta'

    id = Column(Integer, primary_key=True, index=True)
    nama_kereta = Column(String(100))
    stasiun_asal = Column(String(100))
    stasiun_tujuan = Column(String(100))
    waktu_keberangkatan = Column(String(50))
    waktu_tiba = Column(String(50))
    harga = Column(Integer)

Class JadwalKereta adalah definisi model untuk tabel "jadwal_kereta" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas JadwalKereta:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**nama_kereta**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama kereta pada jadwal tersebut.

**stasiun_asal**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama stasiun asal pada jadwal kereta.

**stasiun_tujuan**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama stasiun tujuan pada jadwal kereta.

**waktu_keberangkatan**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan waktu keberangkatan pada jadwal kereta.

**waktu_tiba**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan waktu tiba pada jadwal kereta.

**harga**: Kolom ini bertipe Integer dan digunakan untuk menyimpan harga tiket pada jadwal kereta.

Dengan definisi ini, dapat menggunakan kelas JadwalKereta untuk memetakan data jadwal kereta ke dalam tabel "jadwal_kereta" dalam database. Setiap atribut dalam kelas JadwalKereta akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class Kereta
    class Kereta(Base):
    __tablename__ = 'kereta'

    id = Column(Integer, primary_key=True, index=True)
    nama_kereta = Column(String(100))
    kapasitas = Column(String(100))
    jenis_kereta = Column(String(100))
    kelas_kereta = Column(String(100))

Class Kereta yang adalah definisi model untuk tabel "kereta" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas Kereta:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**nama_kereta**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama kereta.

**kapasitas**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan kapasitas kereta.

**jenis_kereta**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan jenis kereta, seperti eksekutif, bisnis, atau ekonomi.

**kelas_kereta**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan kelas kereta, seperti kelas 1, kelas 2, atau kelas 3.

Dengan definisi ini, dapat menggunakan kelas Kereta untuk memetakan data kereta ke dalam tabel "kereta" dalam database. Setiap atribut dalam kelas Kereta akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class Stasiun
    class Stasiun(Base):
    __tablename__ = "stasiun"

    id = Column(Integer, primary_key=True, index=True)
    nama = Column(String)
    kota = Column(String)
    provinsi = Column(String)

Class Stasiun yang adalah definisi model untuk tabel "stasiun" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas Stasiun:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**nama**: Kolom ini bertipe String (defaultnya adalah String(255)) dan digunakan untuk menyimpan nama stasiun.

**kota**: Kolom ini bertipe String (defaultnya adalah String(255)) dan digunakan untuk menyimpan nama kota di mana stasiun berada.

**provinsi**: Kolom ini bertipe String (defaultnya adalah String(255)) dan digunakan untuk menyimpan nama provinsi di mana stasiun berada.

Dengan definisi ini, dapat menggunakan kelas Stasiun untuk memetakan data stasiun ke dalam tabel "stasiun" dalam database. Setiap atribut dalam kelas Stasiun akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class RiwayatPembelian
    class RiwayatPembelian(Base):
    __tablename__ = 'riwayat_pembelian'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    tiket_id = Column(Integer)
    stasiunAsal = Column(String(100))
    stasiunTujuan = Column(String(100))
    tanggalKeberangkatan = Column(String(50))
    jumlahPenumpang = Column(Integer)
    usia = Column(Integer)
    total_harga = Column(Integer)
    noIdentitas = Column(String(255))

Class RiwayatPembelian adalah definisi model untuk tabel "riwayat_pembelian" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas RiwayatPembelian:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**user_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID pengguna yang terkait dengan riwayat pembelian.

**tiket_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID tiket yang terkait dengan riwayat pembelian.

**stasiunAsal**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama stasiun asal pada riwayat pembelian.

**stasiunTujuan**: Kolom ini bertipe String(100) dan digunakan untuk menyimpan nama stasiun tujuan pada riwayat pembelian.

**tanggalKeberangkatan**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan tanggal keberangkatan pada riwayat pembelian.

**jumlahPenumpang**: Kolom ini bertipe Integer dan digunakan untuk menyimpan jumlah penumpang pada riwayat pembelian.

**usia**: Kolom ini bertipe Integer dan digunakan untuk menyimpan usia penumpang pada riwayat pembelian.

**total_harga**: Kolom ini bertipe Integer dan digunakan untuk menyimpan total harga pembelian pada riwayat pembelian.

**noIdentitas**: Kolom ini bertipe String(255) dan digunakan untuk menyimpan nomor identitas penumpang pada riwayat pembelian.

Dengan definisi ini, dapat menggunakan Class RiwayatPembelian untuk memetakan data riwayat pembelian ke dalam tabel "riwayat_pembelian" dalam database. Setiap atribut dalam kelas RiwayatPembelian akan menjadi kolom dalam tabel dengan nama yang sesuai.

Class TopUpKaiPay
    class TopUpKaiPay(Base):
    __tablename__ = "topup_kaipay"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer)
    jumlah_topup = Column(Float)
    tanggal_transaksi = Column(String(50))

Class TopUpKaiPay adalah definisi model untuk tabel "topup_kaipay" dalam database. Berikut ini adalah penjelasan untuk setiap kolom dalam kelas TopUpKaiPay:

**id**: Kolom ini bertipe Integer dan memiliki atribut primary_key=True, yang menandakan bahwa kolom ini adalah kunci primer dalam tabel. Kunci primer adalah nilai unik yang secara unik mengidentifikasi setiap baris dalam tabel. Atribut index=True menunjukkan bahwa indeks akan dibuat untuk kolom ini, yang akan meningkatkan performa pada operasi pencarian berdasarkan kolom ini.

**user_id**: Kolom ini bertipe Integer dan digunakan untuk menyimpan ID pengguna yang terkait dengan top-up KAI Pay.

**jumlah_topup**: Kolom ini bertipe Float dan digunakan untuk menyimpan jumlah top-up yang dilakukan pada KAI Pay.

**tanggal_transaksi**: Kolom ini bertipe String(50) dan digunakan untuk menyimpan tanggal transaksi top-up KAI Pay.

Dengan definisi ini, dapat menggunakan Class TopUpKaiPay untuk memetakan data top-up KAI Pay ke dalam tabel "topup_kaipay" dalam database. Setiap atribut dalam kelas TopUpKaiPay akan menjadi kolom dalam tabel dengan nama yang sesuai.

Berikut program lengkapnya [Aktivitas Database Web Service KAI Access](Source Code/models.py)

# No.6
**Mampu menunjukkandan menjelaskan pembuatan web service setiap operasi CRUD nya**
Saya akan mendemonstrasikan dan menjelaskan pembuatan web service dengan operasi CRUD menggunakan library FastAPI dalam Python. FastAPI adalah framework web modern dan cepat untuk membuat layanan API dengan mudah. Berikut adalah contoh implementasi sederhana dari web service dengan operasi CRUD (Create, Read, Update, Delete) menggunakan FastAPI
- Instalasi Dependensi:
Pastikan Anda telah menginstal FastAPI dan library pendukungnya dengan menjalankan perintah berikut di terminal
pip install fastapi
pip install sqlalchemy
pip install databases
pip install uvicorn
- Impor library yang diperlukan dan buat instansi aplikasi FastAPI

from fastapi import FastAPI, HTTPException, Depends, status
from pydantic import BaseModel
from typing import Annotated
import models
from database import engine, SessionLocal
from sqlalchemy.orm import Session

- Konfigurasi Koneksi Database:
Definisikan URL koneksi ke database dan buat objek Database serta objek sesi SQLAlchemy [Koneksi Database](Source Code/models.py)
- Definisikan model data:
Buat struktur data untuk entitas yang akan disimpan dan dikelola oleh web service. Sebagai contoh, mari kita anggap kita ingin membuat web service untuk mengelola daftar pengguna (user) dengan atribut id, nama, dan email. Tambahkan kode berikut di bawah definisi aplikasi FastAPI. Berikut program [model data](Source Code/models.py)


# No.7
**Mampu menunjukkandan menjelaskan grapichal user interface dari produk digital**
Berikut ini adalah penjelasan mengenai grapichal user interface yang saya buat untuk aplikasi KAI Access. dari beberapa tampilan saya membuat tiga gui yang pertama untuk registrasi, login, dan pesan tiket.
1. Registrasi
    - GUI registrasi memiliki beberapa elemen-elemen dan kotak teks menggunakan kelas-kelas seperti JTextField, JDateChoose, JPasswordField, JComboBox.
    - Elemen-elemen tersebut akan meminta pengguna untuk memasukkan biodata atau informasi pribadi seperti, nama lengkap, nomor identitas, email, nomor hp, jenis kelamin, jenis kelamin, tipe identitas, email, password, dan konfirmasi password.
    - Terdapat tombol button menggunakan JButton untuk registrasi, login untuk menuju halaman login, dan beranda untuk menuju halaman pesan tiket. 
    - Apabila registrasi berhasil, maka akan menampilkan pesan menggunakan JOptionPane
    - Jika terjadi kesalahan valisasi atai registrasi, pesan kesalahan akan ditampilkan menggunakan JOptionPane.

Berikut program lengkapnya
[Program GUI Login](GUI Java Swing/Login.java) &
[Design GUI Login](GUI Java Swing/Login.java)

2. Login
    - GUI Login memiliki beberapa elemen-elemen seperti label, kotak teks, dan kotak sandi menggunakan JLabel, JTextField, dan JPasswordField.
    - Pengguna diminta untuk memasukkan username dan password 
    - Terdapat tombol button login menggunakan kelas JButton yang akan memvalidasi informasi masukan dengan data yang ada diserver atau database.
    - Selain itu terdapat tombol button beranda yang menunjukkan halaman pesan tiket.
    - Apabila belum melakukan registrasi terdapat tombol button registrasi pada pojok kiri atas yang akan langsung menampilakn form registrasi.
    - Jika informasi valid, maka akan ditampilakn  "Berhasil login! menggunakan JOptionPane.
    - Apabila informasi salah akan menampilkan pesan "Login gagal. username datau password salah" menggunakan JOptionPane

Berikut program lengkapnya
[Program GUI Registrasi](GUI Java Swing/PesanTiket.java) &
[Design GUI Registrasi](GUI Java Swing/Registrasi.form)

3. Pesan tiket
    - GUI Pesan Tiket memiliki beberapa elemen seperti label, daftar stasiun menggunakan JComboBox, usia menggunakan JComboBox- 
    - Pengguna stasiun asal, stsiun tujuan, usia, nomo identitas.
    - Ketika tombol button diklik, akan menampilkan "Tiket telah dipesan" menggunakan JOptionPane.
    
Berikut program lengkapnya 
[Program GUI Pesan Tiket](GUI Java Swing/PesanTiket.java) &
[Design GUI PEsan Tiket](GUI Java Swing/Login.form)

# No.8
**Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital**

Berikut adalah penjelasan rinci tentang kode contoh untuk menunjukkan dan menjelaskan koneksi HTTP melalui GUI Java Swing:

1. Mengimpor Kebutuhan Dasar
   ```java
   import javax.swing.*;
   import java.awt.*;
   import java.awt.event.ActionEvent;
   import java.awt.event.ActionListener;
   import java.net.http.HttpClient;
   import java.net.http.HttpRequest;
   import java.net.http.HttpResponse;
   import java.net.URI;
   ```
   Di bagian ini, kita mengimpor kelas-kelas yang diperlukan untuk membuat antarmuka pengguna (GUI) dengan Java Swing, serta kelas-kelas yang terkait dengan koneksi HTTP menggunakan pustaka HttpClient.

2. Membuat Kelas `HttpConnectionGUI`
   ```java
   public class HttpConnectionGUI extends JFrame {
       // Deklarasi komponen GUI
       private JTextField urlField;
       private JTextArea responseArea;

       public HttpConnectionGUI() {
           // Pengaturan jendela
           setTitle("HTTP Connection");
           setSize(400, 300);
           setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

           // Inisialisasi komponen GUI
           urlField = new JTextField();
           JButton sendButton = new JButton("Send Request");
           responseArea = new JTextArea();

           // Menambahkan ActionListener untuk tombol "Send Request"
           sendButton.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                   String url = urlField.getText();
                   sendHttpRequest(url);
               }
           });

           // Mengatur tata letak antarmuka pengguna
           setLayout(new BorderLayout());
           JPanel inputPanel = new JPanel(new BorderLayout());
           inputPanel.add(new JLabel("URL: "), BorderLayout.WEST);
           inputPanel.add(urlField, BorderLayout.CENTER);
           inputPanel.add(sendButton, BorderLayout.EAST);

           JScrollPane responseScrollPane = new JScrollPane(responseArea);

           add(inputPanel, BorderLayout.NORTH);
           add(responseScrollPane, BorderLayout.CENTER);

           setVisible(true);
       }

       // Metode untuk mengirim permintaan HTTP
       private void sendHttpRequest(String url) {
           // Membuat objek HttpClient
           HttpClient client = HttpClient.newHttpClient();
           // Membuat objek HttpRequest dengan URL yang diberikan
           HttpRequest request = HttpRequest.newBuilder()
                   .uri(URI.create(url))
                   .build();

           try {
               // Mengirim permintaan HTTP dan menerima respons
               HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
               // Mendapatkan status kode dari respons
               int statusCode = response.statusCode();
               // Mendapatkan isi respons
               String responseBody = response.body();
               // Menggabungkan status kode dan isi respons untuk ditampilkan
               String result = "Status Code: " + statusCode + "\n\n" + responseBody;
               // Menampilkan respons di JTextArea
               responseArea.setText(result);
           } catch (Exception e) {
               // Menampilkan pesan kesalahan jika terjadi exception
               responseArea.setText("Error occurred: " + e.getMessage());
           }
       }

       public static void main(String[] args) {
           SwingUtilities.invokeLater(new Runnable() {
               public void run() {
                   new HttpConnectionGUI();
               }
           });
       }
   }
   ```
   - Di dalam kelas `HttpConnectionGUI`, mendeklarasikan komponen-komponen GUI yang akan digunakan, seperti `JTextField` untuk memasukkan URL dan `JTextArea` untuk menampilkan respons HTTP.
   - Pada konstruktor `HttpConnectionGUI`, mengatur judul jendela, ukuran jendela, dan mengatur operasi penutupan jendela saat tombol keluar diklik.
   - Saya menginisialisasi komponen GUI yang diperlukan dan menambahkan `ActionListener` ke tombol "Send Request". Ketika tombol tersebut diklik, metode `sendHttpRequest()` akan dipanggil.
   - Saya mengatur tata letak GUI dengan menggunakan `BorderLayout` dan mengatur panel input dengan `JLabel`, `JTextField`, dan tombol "Send Request". Kami juga menggunakan `JScrollPane` untuk `JTextArea` agar dapat menggulirkan respons jika terlalu panjang.
   - Di dalam metode `sendHttpRequest()`, saya membuat objek `HttpClient` dan objek `HttpRequest` dengan URL yang diberikan. Kemudian, saya menggunakan `HttpClient` untuk mengirim permintaan HTTP dan menerima respons dari server.
   - Lalu mendapatkan status kode dari respons menggunakan `response.statusCode()`, dan isi respons menggunakan `response.body()`. Kemudian, kami menggabungkan status kode dan isi respons untuk ditampilkan di `JTextArea`.
   - Jika terjadi kesalahan dalam mengirim permintaan HTTP, seperti koneksi gagal atau URL yang tidak valid, pesan kesalahan akan ditampilkan di `JTextArea`.

3. Metode `main()` sebagai Titik Masuk Program
   ```java
   public static void main(String[] args) {
       SwingUtilities.invokeLater(new Runnable() {
           public void run() {
               new HttpConnectionGUI();
           }
       });
   }
   ```
   Metode `main()` adalah titik masuk program. Di dalamnya, kami menggunakan `SwingUtilities.invokeLater()` untuk memastikan pembuatan dan tampilan jendela GUI terjadi pada _Event Dispatch Thread_ yang benar.

Setelah Anda menyimpan kode di atas dalam file `HttpConnectionGUI.java`, dapat mengkompilasi kode dengan menjalankan perintah `javac HttpConnectionGUI.java` di terminal. Jika kompilasi berhasil, dapat menjalankan aplikasi dengan perintah `java HttpConnectionGUI`. Ini akan membuka jendela aplikasi GUI, di mana dapat memasukkan URL dan mengklik tombol "Send Request" untuk mengirim permintaan HTTP dan melihat responsnya di `JTextArea`.

Berikut program lengkapnya [HTTP ke GUI Java Swing](Source Code/HttpConnectionGUI.java)

# No.9
**Mampu mendemostrasikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video youtube**
[Link Youtube](https://youtube.com/watch?v=_ejxQLCDvQ8&feature=share7)
# No.10
**BONUS!! mendemostrasikan pengguna machine learning**
> 




